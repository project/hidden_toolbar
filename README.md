CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Attention

INTRODUCTION
------------

Hidden Toolbar for Drupal.

 * For a full description of the module visit:
   https://www.drupal.org/project/hidden_toolbar

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/hidden_toolbar

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install the Hidden Toolbar module as you would normally
   install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------
  * Configure Hidden Toolbar Settings in
  Administration » Configuration » System » Administration Menu:

   - Choose side of burger menu
   - Customize color scheme
   - Save configuration or Reset to default


MAINTAINERS
-----------

Current maintainers:
 * Alexandre Dias (Saidatom) - https://www.drupal.org/u/saidatom
 * Paulo Nunes (syndicatefx) - https://www.drupal.org/u/syndicatefx
 * Miguel Leal (miguel.leal) - https://www.drupal.org/u/miguelleal

ATTENTION
---------

Most bugs have been ironed out, holes covered, features added. This module
is a work in progress. Please report bugs and suggestions, ok?
