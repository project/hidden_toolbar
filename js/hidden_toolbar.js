/**
 * @file
 * Hide and Show toolbar.
 */
(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.HiddenToolbar = {
    attach: function attach(context) {
      $(context).find('#hidden-toolbar-administration').once('hidden_toolbar').each(function () {
        var menuBtn = document.querySelector('.htb-menu-btn');
        var nav = document.querySelector('.hidden-toolbar-menu');
        var link = document.querySelector('.htb-nav-links');
        var page = document.getElementsByTagName( 'html' )[0];

        function togglehtbMenu(nav, link, page) {
          menuBtn.classList.toggle('active');
          nav.classList.toggle('nav-open');
          link.classList.toggle('fade-in');
          page.classList.toggle('no-scroll');
        };
        
        menuBtn.addEventListener('click', () => {
          togglehtbMenu(nav, link, page);
        });
      });
    }
  };
})(jQuery, Drupal, drupalSettings);


