<?php

namespace Drupal\hidden_toolbar\Form;

use \Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class HiddenToolbarForm.
 */
class HiddenToolbarForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hidden_toolbar_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hidden_toolbar.settings');
    $form['choose_side'] = [
      '#type' => 'radios',
      '#title' => $this->t('Choose side of burger menu'),
      '#options' => [
        'tright' => $this->t('Top/Right'),
        'bright' => $this->t('Bottom/Right'),
      ],
      '#default_value' => $config->get('choose_side'),
    ];
    $form['menu_color'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Customize color scheme'),
    ];
    $form['menu_color']['bg-color'] = [
      '#type' => 'color',
      '#title' => $this->t('Background color'),
      '#default_value' => $config->get('active.bg-color'),
    ];
    $form['menu_color']['bg-color-active'] = [
      '#type' => 'color',
      '#title' => $this->t('Background active color'),
      '#default_value' => $config->get('active.bg-color-active'),
    ];
    $form['menu_color']['bg-color-hover'] = [
      '#type' => 'color',
      '#title' => $this->t('Background hover color'),
      '#default_value' => $config->get('active.bg-color-hover'),
    ];
    $form['menu_color']['border-color'] = [
      '#type' => 'color',
      '#title' => $this->t('Border color'),
      '#default_value' => $config->get('active.border-color'),
    ];
    $form['menu_color']['text-color'] = [
      '#type' => 'color',
      '#title' => $this->t('Text color'),
      '#default_value' => $config->get('active.text-color'),
    ];
    $form['menu_color']['text-color-active'] = [
      '#type' => 'color',
      '#title' => $this->t('Text active color'),
      '#default_value' => $config->get('active.text-color-active'),
    ];

    $form['menu_color']['reset_button'] = [
      '#type' => 'button',
      '#value' => t('button reset to default theme'),
      '#ajax'        => [
        'callback' => [$this, 'resetHiddenToolbarColors'],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('hidden_toolbar.settings')
      ->set('choose_side', $form_state->getValue('choose_side'))
      ->set('active.bg-color', $form_state->getValue('bg-color'))
      ->set('active.bg-color-active', $form_state->getValue('bg-color-active'))
      ->set('active.bg-color-hover', $form_state->getValue('bg-color-hover'))
      ->set('active.border-color', $form_state->getValue('border-color'))
      ->set('active.text-color', $form_state->getValue('text-color'))
      ->set('active.text-color-active', $form_state->getValue('text-color-active'))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'hidden_toolbar.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function resetHiddenToolbarColors(array &$form, FormStateInterface $form_state) {
    $config = $this->config('hidden_toolbar.settings');
    $this->config('hidden_toolbar.settings')
      ->set('choose_side', $config->get('choose_side'))
      ->set('active.bg-color', $config->get('default.bg-color'))
      ->set('active.bg-color-active', $config->get('default.bg-color-active'))
      ->set('active.bg-color-hover', $config->get('default.bg-color-hover'))
      ->set('active.border-color', $config->get('default.border-color'))
      ->set('active.text-color', $config->get('default.text-color'))
      ->set('active.text-color-active', $config->get('default.text-color-active'))
      ->save();

    \Drupal::messenger()->addMessage($this->t('Default theme restored'), 'status', TRUE);

    $message = [
      '#theme' => 'status_messages',
      '#message_list' => \Drupal::messenger()->all(),
    ];

    $messages = \Drupal::service('renderer')->render($message);

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#result-message', $messages));
    $currentURL = Url::fromRoute('hidden_toolbar.hidden_toolbar_form');
    $response->addCommand(new RedirectCommand($currentURL->toString()));
    return $response;
  }

}
