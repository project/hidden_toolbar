<?php

namespace Drupal\hidden_toolbar\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Render\Element;

/**
 * Provides a render element for the default Drupal hidden_toolbar.
 *
 * @RenderElement("hidden_toolbar")
 */
class HiddenToolbar extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
      '#pre_render' => [
      [$class, 'preRenderToolbar'],
      ],
      '#theme' => 'hidden_toolbar',
      '#attached' => [
        'library' => [
          'hidden_toolbar/hidden_base',
        ],
      ],
      // Metadata for the hidden_toolbar wrapping element.
      '#attributes' => [
      // The id cannot be simply "hidden_toolbar" or it will clash with the
      // simpletest tests listing which produces a checkbox with attribute
      // id="hidden_toolbar".
        'id' => 'hidden-toolbar-administration',
        'role' => 'group',
        'aria-label' => $this->t('Site administration hidden toolbar'),
      ],
      // Metadata for the administration bar.
      '#bar' => [
        '#heading' => $this->t('Hidden toolbar items'),
        '#attributes' => [
          'id' => 'hidden-toolbar-bar',
          'role' => 'navigation',
          'aria-label' => $this->t('Hidden toolbar items'),
        ],
      ],
    ];
  }

  /**
   * Builds the Toolbar as a structured array ready for drupal_render().
   *
   * Since building the hidden_toolbar takes some time, it is done just prior to
   * rendering to ensure that it is built only if it will be displayed.
   *
   * @param array $element
   *   A renderable array.
   *
   * @return array
   *   A renderable array.
   *
   * @see toolbar_page_top()
   */
  public static function preRenderToolbar($element) {
    $module_handler = static::moduleHandler();
    // Get toolbar items from all modules that implement hook_toolbar().
    $items = $module_handler->invokeAll('hidden_toolbar');
    // Allow for altering of hook_toolbar().
    $module_handler->alter('hidden_toolbar', $items);
    // Sort the children.
    uasort($items, ['\Drupal\Component\Utility\SortArray', 'sortByWeightProperty']);

    // Merge in the original toolbar values.
    $element = array_merge($element, $items);

    // Assign each item a unique ID, based on its key.
    foreach (Element::children($element) as $key) {
      $element[$key]['#id'] = Html::getId('hidden-toolbar-item-' . $key);
    }

    return $element;
  }

  /**
   * Wraps the module handler.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler.
   */
  protected static function moduleHandler() {
    return \Drupal::moduleHandler();
  }

}
