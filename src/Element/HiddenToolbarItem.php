<?php

namespace Drupal\hidden_toolbar\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Url;

/**
 * Provides a hidden_toolbar item that is wrapped in markup for common styling.
 *
 * The 'tray' property contains a renderable array.
 *
 * @RenderElement("hidden_toolbar_item")
 */
class HiddenToolbarItem extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRenderToolbarItem'],
      ],
      'tab' => [
        '#type' => 'link',
        '#title' => NULL,
        '#url' => Url::fromRoute('<front>'),
      ],
    ];
  }

  /**
   * Provides markup for associating a tray trigger with a tray element.
   *
   * A tray is a responsive container that wraps renderable content. Trays
   * present content well on small and large screens alike.
   *
   * @param array $element
   *   A renderable array.
   *
   * @return array
   *   A renderable array.
   */
  public static function preRenderToolbarItem($element) {
    return $element;
  }

}
